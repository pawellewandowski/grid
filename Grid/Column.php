<?php

namespace PedroTeixeira\Bundle\GridBundle\Grid;

/**
 * Grid Column
 */
class Column
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $field;

    /**
     * @var string
     */
    protected $index;

    /**
     * @var string
     */
    protected $twig;


    /**
     * @var array
     */
    protected $twigParams;

    protected $classes;
    /**
     * @var bool
     */
    protected $sortable = true;

    /**
     * @var string
     */
    protected $filterType = 'text';

    /**
     * @var \PedroTeixeira\Bundle\GridBundle\Grid\Filter\FilterAbstract
     */
    protected $filter;

    /**
     * @var string
     */
    protected $renderType = 'text';

    /**
     * @var string
     */
    protected $renderFunction = false;

    /**
     * @var bool
     */
    protected $exportOnly = false;

    /**
     * @var \PedroTeixeira\Bundle\GridBundle\Grid\Render\RenderAbstract
     */
    protected $render;

    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    protected $container;

    protected $slugify;

    protected $indexes;

    /**
     * @param \Symfony\Component\DependencyInjection\Container $container
     * @param string $name
     */
    public function __construct(\Symfony\Component\DependencyInjection\Container $container, $name = '')
    {
        $this->container = $container;
        $this->name = $name;
        $this->twigParams = [];
        $this->classes = [];
        $this->slugify = false;
        $this->indexes = [];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Column
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getField()
    {
        if (empty($this->field)) {
            $this->field = uniqid();
        }

        return $this->field;
    }

    /**
     * @param string $field
     *
     * @return Column
     */
    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * @return string
     */
    public function getTwig()
    {
        return $this->twig;
    }

    /**
     * @param string $twig
     *
     * @return Column
     */
    public function setTwig($twig)
    {
        $this->twig = $twig;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getSortable()
    {
        return $this->sortable;
    }

    /**
     * @param bool $sortable
     *
     * @return Column
     */
    public function setSortable($sortable)
    {
        $this->sortable = $sortable;

        return $this;
    }

    /**
     * @return null|string
     */
    public function renderFilter()
    {
        if ($this->getFilter()) {
            return $this->getFilter()->render();
        }

        return null;
    }

    /**
     * Return filter
     *
     * @todo Merge all the reflections in one single method
     *
     * @return \PedroTeixeira\Bundle\GridBundle\Grid\Filter\FilterAbstract|bool
     *
     * @throws \Exception
     */
    public function getFilter()
    {
        if ($this->filter) {
            return $this->filter;
        }

        $filterType = $this->getFilterType();

        if (!$filterType) {
            return false;
        }

        if (class_exists($filterType)) {
            $className = $filterType;
        } else {
            $className = str_replace('_', ' ', $filterType);
            $className = ucwords(strtolower($className));
            $className = str_replace(' ', '', $className);

            $className = 'PedroTeixeira\Bundle\GridBundle\Grid\Filter\\' . $className;
        }

        try {
            $reflection = new \ReflectionClass($className);

            $this->filter = $reflection->newInstance(
                $this->container
            );

            $this->filter->setIndex($this->getIndex());
            $this->filter->setIndexes($this->getIndexes());

            return $this->filter;

        } catch (\Exception $e) {
            throw new \Exception(
                sprintf('Grid column type "%s" doesn\'t exist', $filterType)
            );
        }
    }

    /**
     * @return string
     */
    public function getFilterType()
    {
        return $this->filterType;
    }

    /**
     * @param string $filterType
     *
     * @return Column
     */
    public function setFilterType($filterType)
    {
        $this->filterType = $filterType;

        return $this;
    }

    /**
     * @return string
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param string $index
     *
     * @return Column
     */
    public function setIndex($index)
    {
        $this->index = $index;

        return $this;
    }

    public function getIndexes()
    {
        return $this->indexes;
    }

    /**
     * @return boolean
     */
    public function getExportOnly()
    {
        return $this->exportOnly;
    }

    /**
     * @param boolean $exportOnly
     *
     * @return Column
     */
    public function setExportOnly($exportOnly)
    {
        $this->exportOnly = $exportOnly;

        return $this;
    }

    /**
     * Return render
     *
     * @todo Merge all the reflections in one single method
     *
     * @return \PedroTeixeira\Bundle\GridBundle\Grid\Render\RenderAbstract|bool
     *
     * @throws \Exception
     */
    public function getRender()
    {
        if ($this->render) {
            return $this->render;
        }

        $renderType = $this->getRenderType();

        if (!$renderType) {
            return false;
        }

        if (class_exists($renderType)) {
            $className = $renderType;
        } else {
            $className = str_replace('_', ' ', $renderType);
            $className = ucwords(strtolower($className));
            $className = str_replace(' ', '', $className);

            $className = 'PedroTeixeira\Bundle\GridBundle\Grid\Render\\' . $className;
        }

        try {
            $reflection = new \ReflectionClass($className);

            $this->render = $reflection->newInstance(
                $this->container
            );

            return $this->render;
        } catch (\Exception $e) {
            throw new \Exception(
                sprintf('Grid render type "%s" doesn\'t exist', $renderType)
            );
        }
    }

    /**
     * @return string
     */
    public function getRenderType()
    {
        return $this->renderType;
    }

    /**
     * @param string $renderType
     *
     * @return Column
     */
    public function setRenderType($renderType)
    {
        $this->renderType = $renderType;

        return $this;
    }

    /**
     * @return string
     */
    public function getRenderFunction()
    {
        return $this->renderFunction;
    }

    /**
     * @param string $renderFunction
     */
    public function setRenderFunction($renderFunction)
    {
        $this->renderFunction = $renderFunction;

        return $this;
    }

    /**
     * @return array
     */
    public function getTwigParams()
    {
        return $this->twigParams;
    }

    /**
     * @param array $twigParams
     */
    public function setTwigParams($twigParams)
    {
        $this->twigParams = $twigParams;
    }

    public function getClasses()
    {
        return $this->classes;
    }

    /**
     * @param array $twigParams
     */
    public function setClasses($class)
    {
        $this->classes = $class;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSlugify(): bool
    {
        return $this->slugify;
    }

    /**
     * @param bool $slugify
     */
    public function setSlugify(bool $slugify)
    {
        $this->slugify = $slugify;
        return $this;
    }

    public function addIndex(string $index)
    {
        $this->indexes[] = $index;
        return $this;
    }
}

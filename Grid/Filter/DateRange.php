<?php

namespace PedroTeixeira\Bundle\GridBundle\Grid\Filter;

/**
 * Filter DateRange
 */
class DateRange extends Date
{
    /**
     * @var string
     */
    protected $operatorType = 'date_range';

    /**
     * @var string
     */
    protected $inputSeparator = ' : ';

    /**
     * @return string
     */
    public function render()
    {
        if ($this->getUseDatePicker()) {
            $html = '<div class="g-flex-centered">';
            $html .= '<input name="' . $this->getIndex() . '[]" id="' . $this->getId() .
                'from" class="datepicker-enter form-control g-width-70 grid-data pull-left" data-provide="datepicker" type="text" value="' . $this->getValue() .
                '" placeholder="' . $this->getPlaceholder() .
                '" data-date-format="' . $this->dateFormat . '">';
            $html .= '<div class="pull-left g-mx-5 g-mt-2"> - </div>';
            $html .= '<input name="' . $this->getIndex() . '[]" id="' . $this->getId() .
                'to" class="datepicker-enter pull-left form-control g-width-70 grid-data" data-provide="datepicker" type="text" value="' . $this->getValue() .
                '" placeholder="' . $this->getPlaceholder() .
                '" data-date-format="' . $this->dateFormat . '">';
            $html .= '</div>';
        } else {
            $html = '<input class="date-input" name="' . $this->getIndex() . '[]" id="' . $this->getId() .
                'from" type="date" placeholder="' . $this->getPlaceholder() . '" value="' . $this->getValue() . '"> ';

            $html .= $this->getInputSeparator();

            $html .= '<input class="date-input" name="' . $this->getIndex() . '[]" id="' . $this->getId() .
                'to" type="date" placeholder="' . $this->getPlaceholder() . '" value="' . $this->getValue() . '">';
            $html .= '</div>';
        }

        return $html;
    }

    /**
     * @return string
     */
    public function getInputSeparator()
    {
        return $this->inputSeparator;
    }

    /**
     * @param string $inputSeparator
     *
     * @return DateRange
     */
    public function setInputSeparator($inputSeparator)
    {
        $this->inputSeparator = $inputSeparator;

        return $this;
    }
}

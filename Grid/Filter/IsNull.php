<?php

namespace PedroTeixeira\Bundle\GridBundle\Grid\Filter;

/**
 * Filter IsNull
 */
class IsNull extends Select
{
    /**
     * @var string
     */
    protected $operatorType = 'is_null';
    /**
     * @var array
     */
    protected $options = array(
        0 => 'Nie dodano',
        1 => 'Dodano'
    );
}

<?php

namespace PedroTeixeira\Bundle\GridBundle\Grid\Filter;

/**
 * Filter NumberRange
 */
class NumberRange extends FilterAbstract
{
    /**
     * @var string
     */
    protected $operatorType = 'number_range';

    /**
     * @var string
     */
    protected $inputSeparator = ' : ';

    protected $defaultStartValue = 0;

    /**
     * @return string
     */
    public function render()
    {
        $html = '<div class="g-flex-centered g-min-width-230">';
        $html .= 'od <input class="number-input form-control"  style="margin: 0px 10px;" name="' . $this->getIndex() . '[]" id="' . $this->getId() .
            'from" type="text" placeholder="' . $this->getPlaceholder() . '" value="' . ($this->getValue() ?: $this->defaultStartValue) . '"> ';

        $html .= $this->getInputSeparator();

        $html .= 'do <input class="number-input form-control" style="margin: 0px 10px;" name="' . $this->getIndex() . '[]" id="' . $this->getId() .
            'to" type="text" placeholder="' . $this->getPlaceholder() . '" value="' . $this->getValue() . '"></div>';

        return $html;
    }

    /**
     * @return string
     */
    public function getInputSeparator()
    {
        return $this->inputSeparator;
    }

    /**
     * @param string $inputSeparator
     *
     * @return DateRange
     */
    public function setInputSeparator($inputSeparator)
    {
        $this->inputSeparator = $inputSeparator;

        return $this;
    }

    public function setDefaultStartValue($value)
    {
        $this->defaultStartValue = $value;
    }
}

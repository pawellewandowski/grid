<?php

namespace PedroTeixeira\Bundle\GridBundle\Grid\Filter\Operator;

use Symfony\Component\Locale\Stub\DateFormat\FullTransformer;

/**
 * Date
 */
class Date extends OperatorAbstract
{
    /**
     * @param string|array $value
     */
    public function execute($value)
    {
        $date = new \DateTime($value);

        $queryBuilder = $this->getQueryBuilder();

        $where = $this->getQueryBuilder()->expr()->like($this->getIndex(), ":{$this->getIndexClean()}");

        if ($this->getWhere() == 'OR') {
            $queryBuilder->orWhere($where);
        } else {
            $queryBuilder->andWhere($where);
        }

        $queryBuilder->setParameter($this->getIndexClean(), $date->format('Y-m-d') . '%');
    }
}

<?php

namespace PedroTeixeira\Bundle\GridBundle\Grid\Filter\Operator;

/**
 * Having
 */
class Having extends OperatorAbstract
{
    /**
     * @param array $value
     *
     * @throws \Exception
     */
    public function execute($value)
    {

        $queryBuilder = $this->getQueryBuilder();
        $clearIndex = 'grid';

        if (is_array($value)) {
            if (strlen($value[0]) > 0) {
                $queryBuilder->having($this->getIndex() . " >= :{$clearIndex}")
                    ->setParameter($clearIndex, $value[0]);
            }
            if (array_key_exists(1, $value) && strlen($value[1]) > 0) {
                $queryBuilder->having($this->getIndex() . " <= :{$clearIndex}")
                    ->setParameter($clearIndex, $value[1]);
            }
        } else {
            $queryBuilder->having($this->getIndex() . " = :{$clearIndex}")
                ->setParameter($clearIndex, $value);
        }

    }
}

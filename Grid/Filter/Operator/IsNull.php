<?php

namespace PedroTeixeira\Bundle\GridBundle\Grid\Filter\Operator;

/**
 * Having
 */
class IsNull extends OperatorAbstract
{
    /**
     * @param array $value
     *
     * @throws \Exception
     */
    public function execute($value)
    {


        $queryBuilder = $this->getQueryBuilder();
        $clearIndex = 'grid';

        if ($value === "0") {
            $queryBuilder->where($this->getIndex() . " IS NULL");
        } else {
            $queryBuilder->where($this->getIndex() . " IS NOT NULL");
        }

    }
}

<?php

namespace PedroTeixeira\Bundle\GridBundle\Grid\Filter\Operator;

/**
 * Operator Abstract
 */
abstract class OperatorAbstract
{
    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    protected $container;

    /**
     * @var string
     */
    protected $index;

    protected $indexes;

    /**
     * @var string
     */
    protected $indexClean;

    /**
     * @var \Doctrine\ORM\QueryBuilder
     */
    protected $queryBuilder;

    /**
     * @var string
     */
    protected $where = 'AND';

    /**
     * @param \Symfony\Component\DependencyInjection\Container $container
     */
    public function __construct(\Symfony\Component\DependencyInjection\Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param string|array $value
     *
     * @return void
     */
    abstract public function execute($value);

    /**
     * @return string
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param string $index
     *
     * @return OperatorAbstract
     */
    public function setIndex($index)
    {
        $this->index = $index;

        $this->setIndexClean(
            str_replace('.', '', $index)
        );

        return $this;
    }

    /**
     * @return array
     */
    public function getIndexes()
    {
        return $this->indexes;
    }

    /**
     * @return string
     */
    public function setIndexes(array $indexes)
    {
        $this->indexes = $indexes;
    }

    /**
     * @return string
     */
    public function getIndexClean()
    {
        return $this->indexClean;
    }

    /**
     * @param string $indexClean
     *
     * @return OperatorAbstract
     */
    public function setIndexClean($indexClean)
    {
        $this->indexClean = $indexClean;

        return $this;
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getQueryBuilder()
    {
        return $this->queryBuilder;
    }

    /**
     * @param \Doctrine\ORM\QueryBuilder $queryBuilder
     *
     * @return OperatorAbstract
     */
    public function setQueryBuilder(\Doctrine\ORM\QueryBuilder $queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;

        return $this;
    }

    /**
     * @return string
     */
    public function getWhere()
    {
        return $this->where;
    }

    /**
     * @param string $where
     *
     * @return OperatorAbstract
     */
    public function setWhere($where)
    {
        $this->where = $where;

        return $this;
    }
}

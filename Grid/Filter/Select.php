<?php

namespace PedroTeixeira\Bundle\GridBundle\Grid\Filter;

/**
 * Filter Select
 */
class Select extends FilterAbstract
{
    /**
     * @var array
     */
    protected $options = array();

    /**
     * @var bool
     */
    protected $emptyChoice = true;

    /**
     * @var string
     */
    protected $emptyChoiceLabel = '---';

    protected $multiple = false;

    /**
     * @return string
     */
    public function render()
    {
        $html = '<select ' . $this->getNameAndId($this->multiple) . ' ' . ($this->multiple ? 'multiple' : '') . ' class="select2-disabled form-control">';

        if ($this->getEmptyChoice() && !$this->multiple) {
            $html .= '<option value="" selected>' . $this->translate($this->getEmptyChoiceLabel()) . '</option>';
        }

        if (is_array($this->getOptions())) {
            foreach ($this->getOptions() as $key => $value) {
                $html .= '<option value="' . $key . '">' . $this->translate($value) . '</option>';
            }
        }

        $html .= '</select>';

        return $html;
    }

    /**
     * @return boolean
     */
    public function getEmptyChoice()
    {
        return $this->emptyChoice;
    }

    /**
     * @param bool $emptyChoice
     *
     * @return Select
     */
    public function setEmptyChoice($emptyChoice)
    {
        $this->emptyChoice = $emptyChoice;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmptyChoiceLabel()
    {
        return $this->emptyChoiceLabel;
    }

    /**
     * @param string $emptyChoiceLabel
     *
     * @return Select
     */
    public function setEmptyChoiceLabel($emptyChoiceLabel)
    {
        $this->emptyChoiceLabel = $emptyChoiceLabel;

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param array $options
     *
     * @return Select
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @param bool $multiple
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;
    }
}

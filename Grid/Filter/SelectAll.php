<?php

namespace PedroTeixeira\Bundle\GridBundle\Grid\Filter;

/**
 * Filter Select
 */
class SelectAll extends FilterAbstract
{
    protected $operatorType = 'select_all';

    /**
     * @return string
     */
    public function render()
    {
        return '<input type="checkbox" name="select[]" class="g-align-self-center select-all g-width-18">';
    }

}

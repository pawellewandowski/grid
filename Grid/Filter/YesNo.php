<?php

namespace PedroTeixeira\Bundle\GridBundle\Grid\Filter;

/**
 * Filter YesNo
 */
class YesNo extends Select
{
    
    /**
     * @var array
     */
    protected $options = array(
        '0' => 'nie',
        '1' => 'tak'
    );
}

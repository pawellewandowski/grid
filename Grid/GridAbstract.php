<?php

namespace PedroTeixeira\Bundle\GridBundle\Grid;

use Cocur\Slugify\Slugify;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Response;

/**
 * Grid Abstract
 */
abstract class GridAbstract
{

    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    protected $container;

    /**
     * @var \Symfony\Component\Routing\Router
     */
    protected $router;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     * @var \Doctrine\ORM\QueryBuilder
     */
    protected $queryBuilder;

    /**
     * @var \Twig_TemplateInterface
     */
    protected $templating;

    /**
     * @var array
     */
    protected $columns;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var bool
     */
    protected $ajax;

    /**
     * @var bool
     */
    protected $filterable;

    /**
     * @var bool
     */
    protected $exportable;

    /**
     * @var bool
     */
    protected $export;

    /**
     * @var string
     */
    protected $fileHash;

    /**
     * @var string
     */
    protected $name;

    protected $multiAction;

    protected $limit = false;
    protected $excel;

    /**
     * @param \Symfony\Component\DependencyInjection\Container $container
     *
     * @return \PedroTeixeira\Bundle\GridBundle\Grid\GridAbstract
     */
    public function __construct(\Symfony\Component\DependencyInjection\Container $container )
    {
        $this->container = $container;

        $this->router = $this->container->get('router');
        $this->excel = $this->container->get('phpexcel');
        $this->request = $this->container->get('request_stack')->getCurrentRequest();;
        $this->templating = $this->container->get('twig');

        $this->columns = array();
        $this->url = null;

        $this->ajax = $this->request->isXmlHttpRequest() ? true : false;
        $this->ajaxBoxRequest = $this->request->headers->has('Atmatic-Box-Request') ? true : false;

        $this->exportable = $this->container->getParameter('pedro_teixeira_grid.export.enabled');
        $this->filterable = true;
        $this->export = $this->request->query->get('export', false);
        $this->fileHash = $this->request->query->get('file_hash', null);
        if (is_null($this->fileHash)) {
            $this->fileHash = uniqid();
        }

        $now = new \DateTime();
        $this->name = $this->request->query->get('file_name', md5($now->format('Y-m-d H:i:s:u')));
        $this->multiAction = false;
    }

    /**
     * Setup grid, the place to add columns and options
     */
    abstract public function setupGrid();

    /**
     * @return \Symfony\Component\DependencyInjection\Container
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return bool
     */
    public function isResponseAnswer()
    {
        return ($this->isAjax() || $this->isExport()) && !$this->ajaxBoxRequest;;
    }

    /**
     * @return bool If true (Ajax Request), returns json. Else (Regular request), renders html
     */
    public function isAjax()
    {
        return $this->ajax;
    }

    /**
     * @return bool Check if it's an export call
     */
    public function isExport()
    {
        return $this->export;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        if (!$this->url) {
            $this->url = $this->request->getRequestUri();
        }

        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return GridAbstract
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @param string $name
     *
     * @return Column
     */
    public function addColumn($name)
    {
        $column = new Column($this->container, $name);

        $this->columns[] = $column;

        return $column;
    }

    public function removeColumn($name)
    {
        foreach ($this->columns as $key => $column) {
            if ($column->getName() === $name) {
                unset($this->columns[$key]);
            }
        }
        return $this;
    }

    /**
     * @param $name
     * @param bool $all
     * @return Column
     */
    public function replaceColumn($name, $all = false)
    {
        $newColumn = new Column($this->container, $name);

        $found = false;
        /**
         * @var int $key
         * @var Column $column
         */
        foreach ($this->columns as $key => $column) {
            if ($column->getName() == $name) {
                $this->columns[$key] = $newColumn;
                if (!$all) {
                    return $newColumn;
                }
                $found = true;
            }
        }

        if (!$found) {
            $this->columns[] = $newColumn;
        }

        return $newColumn;
    }

    /**
     * Return columns count
     *
     * @return int
     */
    public function getColumnsCount()
    {
        return count($this->getColumns());
    }

    /**
     * Return an array with column definitions
     *
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * Returns the an array with a GridView instance for normal requests and json for AJAX requests
     *
     * @throws \Exception
     * @return GridView | \Symfony\Component\HttpFoundation\Response
     */
    public function render()
    {
        if ($this->isAjax() && !$this->ajaxBoxRequest) {

            if ($this->isExport()) {
                if (!$this->getExportable()) {
                    throw new \Exception('Export not allowed');
                }
                $data = $this->processExport();
            } else {
                $data = $this->processGrid();
            }

            $json = json_encode($data);

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($json);

            return $response;
        } else {
            if ($this->isExport()) {

                if (!$this->getExportable()) {
                    throw new \Exception('Export not allowed');
                }

                $exportFile = $this->getExportFileName();

                $response = new Response();
                $response->headers->set('Content-Type', 'application/vnd.ms-excel');
                $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($exportFile) . '"');
                $response->setContent(file_get_contents($exportFile));

                return $response;
            } else {
                return new GridView($this, $this->container);
            }
        }
    }

    /**
     * @return boolean
     */
    public function getExportable()
    {
        return $this->exportable;
    }

    /**
     * @param boolean $exportable
     *
     * @return GridAbstract
     */
    public function setExportable($exportable)
    {
        $this->exportable = $exportable;

        return $this;
    }

    /**
     * @return array
     */
    public function processExport()
    {
        set_time_limit(0);

        $exportFile = $this->getExportFileName();


        $columnsHeader = array();

        /** @var Column $column */
        foreach ($this->getColumns() as $column) {
            if (!$column->getTwig()) {
                $columnsHeader[$column->getField()] = $column->getName();
            }
        }

        $phpExcel = $this->excel->createPHPExcelObject();
        $phpExcel->setActiveSheetIndex(0);

        $activeSheet = $phpExcel->getActiveSheet();

        $columnIndex = 0;
        foreach ($columnsHeader as $sOne) {
            $activeSheet->getColumnDimensionByColumn($columnIndex)->setAutoSize(true);
            $activeSheet->setCellValueByColumnAndRow($columnIndex, 1, $sOne);
            $activeSheet->getStyleByColumnAndRow($columnIndex, 1)->getFont()->setBold(true);
            $activeSheet->getStyleByColumnAndRow($columnIndex, 1)->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $columnIndex++;
        }

        $data = $this->getData();
        $row_counter = 2;

        foreach ($data['rows'] as $row) {

            $rowContent = array();
            $counter = 0;
            foreach ($row as $key => $column) {
                if (isset($columnsHeader[$key])) {
                    $activeSheet->setCellValueByColumnAndRow($counter, $row_counter, strip_tags($column));
                    $counter++;
                }
            }
            $row_counter++;
        }

        $writer = $this->excel->createWriter($phpExcel);
        $file = $this->getExportFileName();
        try {
            $writer->save($file);
        } catch (Exception $e) {

        }

        return array(
            'file_hash' => $this->fileHash,
            'file_name' => $this->name
        );
    }

    /**
     * @return string
     */
    protected function getExportFileName()
    {
        $exportPath = $this->container->getParameter('pedro_teixeira_grid.export.path');
        $exportFile = $exportPath . $this->getName() . '_' . $this->fileHash . '.xls';

        return $exportFile;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return GridAbstract
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Process the filters and return the result
     *
     * @return array
     */
    public function getData()
    {

        $defaultLimit = $this->limit ?: $this->container->getParameter('pedro_teixeira_grid.pagination.limit');

        $page = $this->request->query->get('page', 1);
        $limit = $this->request->query->get('limit', $defaultLimit);
        $sortIndex = $this->request->query->get('sort');
        $sortOrder = $this->request->query->get('sort_order');
        $filters = $this->request->query->get('filters', array());

        $page = intval(abs($page));
        $page = ($page <= 0 ? 1 : $page);

        $limit = intval(abs($limit));
        $limit = ($limit <= 0 ? $defaultLimit : $limit);

        /** @todo Remove the unnecessary iterations */

        // Check and change cascade array in get
        foreach ($filters as $key => $filter) {
            if (strpos($filter['name'], '[]') !== false) {

                unset($filters[$key]);
                $name = str_replace('[]', '', $filter['name']);

                if (!isset($filters[$name])) {
                    $filters[$name] = array(
                        'name' => $name,
                        'value' => array($filter['value'])
                    );
                } else {
                    $filters[$name]['value'][] = $filter['value'];
                }
            }
        }

        foreach ($filters as $filter) {
            /** @var \PedroTeixeira\Bundle\GridBundle\Grid\Column $column */
            foreach ($this->columns as $column) {
                if ($filter['name'] == $column->getIndex() && $filter['value'] != '') {
                    if ($column->isSlugify()) {
                        $column->getFilter()->execute($this->getQueryBuilder(),
                            $this->replaceCharacters($filter['value']));
                    } else {
                        $column->getFilter()->execute($this->getQueryBuilder(), $filter['value']);
                    }
                }
            }
        }

        if ($sortIndex) {
            $this->getQueryBuilder()->orderBy($sortIndex, $sortOrder);
        }

        // Don't process grid for export
        if (!$this->isExport()) {
            $totalCount = count(new Paginator($this->getQueryBuilder()->getQuery()));

            $totalPages = ceil($totalCount / $limit);
            $totalPages = ($totalPages <= 0 ? 1 : $totalPages);

            $page = ($page > $totalPages ? $totalPages : $page);

            $queryOffset = (($page * $limit) - $limit);

            $this->getQueryBuilder()
                ->setFirstResult($queryOffset)
                ->setMaxResults($limit);

            $response = array(
                'page' => $page,
                'page_count' => $totalPages,
                'page_limit' => $limit,
                'row_count' => $totalCount,
                'rows' => array()
            );
        } else {
            $response = array(
                'rows' => array()
            );
        }

        foreach ($this->getQueryBuilder()->getQuery()->getResult() as $key => $row) {

            $rowValue = array();

            /** @var Column $column */
            foreach ($this->columns as $column) {

                if ($column->getExportOnly() && !$this->isExport()) {
                    continue;
                }

                $rowColumn = ' ';

                $fields = explode('.', $column->getField());

                $camelCaseField = $this->dashesToCamelCase($fields[0], true);
                // Array
                if ($column->getRenderFunction()) {
                    $func = $column->getRenderFunction();
                    $rowColumn = $func($row);
                } elseif ($column->getTwig()) {
                    $rowColumn = $this->templating->render(
                        $column->getTwig(),
                        array_merge(
                            $column->getTwigParams(),
                            [
                                'row' => $row
                            ]
                        )

                    );
                } elseif (array_key_exists($fields[0], $row)) {
                    $rowColumn = $row[0];
                    // Array scalar
                } elseif (array_key_exists(0, $row) && array_key_exists($fields[0], $row[0])) {
                    $rowColumn = $row[0][$fields[0]];
                    // Object
                } elseif (method_exists($row, 'get' . $camelCaseField)) {
                    $rowColumn = $this->getValue($row, $fields);
                    // Object scalar
                } elseif (array_key_exists(0, $row) && method_exists($row[0], 'get' . $camelCaseField)) {
                    $rowColumn = $this->getValue($row[0], $fields);
                    // Array
                }

                $rowValue[$column->getField()] = $column->getRender()
                    ->setValue($rowColumn)
                    ->setStringOnly($this->isExport())
                    ->render();
            }

            $response['rows'][$key] = $rowValue;
        }

        return $response;
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getQueryBuilder()
    {
        return $this->queryBuilder;
    }

    /**
     * @param \Doctrine\ORM\QueryBuilder $queryBuilder
     *
     * @return GridAbstract
     */
    public function setQueryBuilder(QueryBuilder $queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;

        return $this;
    }

    private function replaceCharacters($string)
    {
        $slugify = new Slugify();
        return trim($slugify->slugify($string, ' '));
    }

    protected function dashesToCamelCase($string, $capitalizeFirstCharacter = false)
    {
        $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));

        if (!$capitalizeFirstCharacter) {
            $str[0] = strtolower($str[0]);
        }

        return $str;
    }

    protected function getValue($obj, $fields)
    {
        $value = $obj;
        foreach ($fields as $field) {
            $method = 'get' . $this->dashesToCamelCase($field, true);
            $value = $value->$method();
        }

        return $value;
    }

    /**
     * @return array
     */
    public function processGrid()
    {
        return $this->getData();
    }

    /**
     * @return boolean
     */
    public function isFilterable()
    {
        return $this->filterable;
    }

    /**
     * @param boolean $filterable
     */
    public function setFilterable($filterable)
    {
        $this->filterable = $filterable;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    public function getMultiAction()
    {
        return $this->multiAction;
    }

    public function isMultiAction()
    {
        return $this->multiAction !== false;
    }

    /**
     * @param boolean $filterable
     */
    public function setMultiAction($multiAction)
    {
        $this->multiAction = $multiAction;
    }
}

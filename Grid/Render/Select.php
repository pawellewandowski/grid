<?php

namespace PedroTeixeira\Bundle\GridBundle\Grid\Render;

/**
 * Render Select
 */
class Select extends RenderAbstract
{
    /**
     * @return string
     */
    public function render()
    {
        return '<input type="checkbox" name="select[]" class="select-checkbox" value="' . $this->getValue() . '">';
    }

}

<?php

namespace PedroTeixeira\Bundle\GridBundle\Twig;

use PedroTeixeira\Bundle\GridBundle\Grid\GridView;
use Twig_Environment;
use Twig_Extension;
use Twig_Extension_InitRuntimeInterface;
use Twig_TemplateInterface;

/**
 * Grid Twig Extension
 */
class GridExtension extends Twig_Extension
{
    /**
     * @var string
     */
    const DEFAULT_TEMPLATE = 'PedroTeixeiraGridBundle::block.html.twig';
    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    protected $container;
    /**
     * @var Twig_Environment
     */
    protected $environment;
    /**
     * @var Twig_TemplateInterface[]
     */
    protected $templates;

    /**
     * Construct
     *
     * @param \Symfony\Component\DependencyInjection\Container $container Container
     */
    public function __construct(\Symfony\Component\DependencyInjection\Container $container)
    {
        $this->container = $container;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return 'pedroteixeira_grid_extension';
    }

    /**
     * Get functions
     *
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('pedroteixeira_grid', [
                $this,
                'renderGrid'

            ],
                ['needs_environment' => true, 'is_safe' => ['html']]),
            new \Twig_SimpleFunction('pedroteixeira_grid_html', [
                $this,
                'renderHtmlGrid'
            ],
                ['needs_environment' => true, 'is_safe' => ['html']]),
            new \Twig_SimpleFunction(
                'pedroteixeira_grid_js', [
                $this,
                'renderJsGrid'
            ],
                ['needs_environment' => true, 'is_safe' => ['html']])
        ];
    }

    /**
     * Render grid view
     *
     * @param GridView $gridView
     *
     * @return mixed
     */
    public function renderGrid(\Twig_Environment $environment, GridView $gridView)
    {
        if (!$gridView->getGrid()->isAjax()) {
            return $this->renderBlock(
                'grid',
                array(
                    'view' => $gridView
                ),
                $environment
            );
        }
    }

    /**
     * Render block
     *
     * @param string $name
     * @param array $parameters
     *
     * @return mixed
     *
     * @throws \InvalidArgumentException
     */
    protected function renderBlock($name, $parameters, $environment)
    {
        $context = array_merge(
            $environment->getGlobals(),
            $parameters
        );
        /** @var Twig_TemplateInterface $template */
        foreach ($this->getTemplates($environment) as $template) {
            if ($template->hasBlock($name, $context)) {

                return $template->renderBlock($name, $context);
            }
        }

        throw new \InvalidArgumentException(sprintf('Block "%s" doesn\'t exist in grid template.', $name));
    }

    /**
     * Render (only html) grid view
     *
     * @param GridView $gridView
     *
     * @return mixed
     */
    public function renderHtmlGrid(\Twig_Environment $environment, GridView $gridView)
    {
        if (!$gridView->getGrid()->isAjax()) {
            return $this->renderBlock(
                'grid_html',
                array(
                    'view' => $gridView
                ),
                $environment
            );
        }
    }

    /**
     * Render (only js) grid view
     *
     * @param GridView $gridView
     *
     * @return mixed
     */
    public function renderJsGrid(\Twig_Environment $environment, GridView $gridView)
    {
        if (!$gridView->getGrid()->isAjax()) {
            return $this->renderBlock(
                'grid_js',
                array(
                    'view' => $gridView
                ),
                $environment
            );
        }
    }

    /**
     * Check if has block
     *
     * @param string $name
     *
     * @return bool
     */
    protected function hasBlock($name)
    {
        /** @var Twig_TemplateInterface $template */
        foreach ($this->getTemplates() as $template) {
            if ($template->hasBlock($name, [])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Template Loader
     *
     * @return Twig_TemplateInterface[]
     *
     * @throws \Exception
     */
    protected function getTemplates($enviroment)
    {
        if (empty($this->templates)) {
            $this->templates[] = $enviroment->loadTemplate($this::DEFAULT_TEMPLATE);
        }

        return $this->templates;
    }
}
